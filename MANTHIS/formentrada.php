<html>

<script language="Javascript">

function validar(datos){
	//comprobando nombre
	if (datos.nom.value.length < 1){
		alert("El campo 'nombre' est� vac�o");
		datos.nom.focus();
		return false;
	}
	var nombre = datos.nom.value;
	var numeros = "0123456789";
	for (var i=0;i<nombre.length; i++)
		for(var j=0; j<numeros.length; j++)
			if(nombre.charAt(i) == numeros.charAt(j)){
				alert("El campo 'nombre' tiene n�meros");
				return false;
			}
	
	//comprobando apellido
	if (datos.ape.value.length < 1){
		alert("El campo 'apellido' est� vac�o");
		datos.ape.focus();
		return false;
	}
	nombre = datos.ape.value;
	for (var i=0;i<nombre.length; i++)
		for(var j=0; j<numeros.length; j++)
			if(nombre.charAt(i) == numeros.charAt(j)){
				alert("El campo 'apellido' tiene n�meros");
				return false;
			}
			
	//comprobando e-mail
	if (datos.mail.value.length < 1){
		alert("El campo 'E-mail' est� vac�o");
		datos.nom.focus();
		return false;
	}
	nombre = datos.mail.value;
	var compr_mail = false;
	for (var i=0;i<nombre.length; i++)
			if(nombre.charAt(i) == '@'){
				compr_mail = true;
			}
	if(compr_mail == false){
		alert("El campo 'E-mail' no tiene '@'");
		return false;
	}

	//comprobando alias
	if (datos.alias.value.length < 1){
		alert("El campo 'alias' est� vac�o");
		datos.nom.focus();
		return false;
	}

	//comprobando contrase�as
	
	var contr = datos.pass.value;
	var recontr = datos.re_pass.value;
	
	
	if (datos.pass.value.length < 1){
		alert("El campo 'Contrase�a' est� vac�o");
		datos.pass.focus();
		return false;
		}
	if (datos.re_pass.value.length < 1){
		alert("El campo de reingreso de contrase�a est� vac�o");
		datos.re_pass.focus();
		return false;
		}
		
	if(!(contr ==recontr)){
		alert("Las contrase�as no coinciden");
		
		return false;
	}
	
	var meses = datos.mes.value;
	var dias = datos.dia.value;
	var anios = datos.ano.value;
	
	
	if (datos.dia.value.length < 1){
		alert("El campo 'D�a' est� vac�o");
		datos.dia.focus();
		return false;
	}

	if ((datos.dia.value > 31) || (datos.dia.value < 0)){
		alert("'D�a' inv�lido");
		datos.dia.focus();
		return false;
	}
	
	if (isNaN(datos.dia.value)){
		alert("El campo 'D�a' no es n�mero");
		return false;
	}
	
	if (datos.mes.value.length < 1){
		alert("El campo 'Mes' est� vac�o");
		datos.mes.focus();
		return false;
	}

	if ((datos.mes.value > 12) || (datos.mes.value < 0)){
		alert("'Mes' inv�lido");
		datos.mes.focus();
		return false;
	}
	
	if (isNaN(datos.mes.value)){
		alert("El campo 'Mes' no es n�mero");
		return false;
	}
	
	if (datos.ano.value.length < 1){
		alert("El campo 'A�o' est� vac�o");
		datos.ano.focus();
		return false;
	}
	var fecha= new Date();
	if ((datos.ano.value > fecha.getFullYear()) || (datos.ano.value < 0)){
		alert("'A�o' inv�lido");
		datos.ano.focus();
		return false;
	}
	
	if (isNaN(datos.ano.value)){
		alert("El campo 'A�o' no es n�mero");
		return false;
	}

	if ((meses == 1) ||(meses == 3) ||(meses == 5) ||(meses == 7) ||(meses == 8) ||(meses == 10) || (meses == 12))
		if((dias > 31) ||(dias < 0)){
		alert("Los dias no coinciden con el mes");
		return false;
		}
	if ((meses == 4) ||(meses == 6) ||(meses == 9) ||(meses == 11))
	if((dias > 30) || (dias < 0)){
		alert("Los dias no coinciden con el mes");
		return false;
		}
	if (meses == 2)
		if((dias > 29) || (dias < 0)){
		alert("Los dias no coinciden con el mes");
		return false;
	}

if (meses == 2) 
	if(dias == 29)
		if(anios %4 != 0){
		alert("Los dias no coinciden con el mes");
		return false;
	}

if( (!(datos.EN.checked)) && (!(datos.FR.checked)) && (!(datos.IT.checked)) && (!(datos.PR.checked)) && (!(datos.DE.checked)) && (!(datos.CH.checked)) && (!(datos.JA.checked)) && (!(datos.KO.checked)) && (!(datos.RU.checked)) ){
		alert("Debe seleccionar al menos un idioma")
		return false;
		}

}




</script>

<td width="50%">
	<h2>Registro</h2><br>

	<form action="registrar.php" onSubmit="return validar(this)" method="post" name="datos">
	<table>
	<tbody><tr>
	<td>Nombre</td>
	<td><input type="text" name="nom" maxlength="60" size="30"></td>
	</tr>
	<tr>
	<td>Apellido</td>
	<td><input type="text" name="ape" maxlength="60" size="30"></td>
	</tr>
	<tr>
	<td>E-Mail</td>
	<td><input type="text" name="mail" maxlength="60" size="30"></td>
	</tr>
	<tr>
	<td>Alias</td>
	<td><input type="text" name="alias" maxlength="60" size="30"></td>
	</tr>
	<tr>
	<td>Contrase�a</td>
	<td><input type="password" name="pass" maxlength="60" size="30"></td>
	</tr>
	<tr>
	<td>Reingresa<br>Contrase�a</td>
	<td><input type="password" name="re_pass" maxlength="60" size="30"></td>
	</tr>
	
	<tr><td>Sexo</td>
	<td><select size="1" name="sexo">
    <option value="m">masculino</option>
    <option value="f">femenino</option>
	</select></td>
	</tr>	
	
	<tr><td>Fecha nacimiento</td>
	<td><br>
    <select name="dia">
	<?php 
	for($i=1;$i<32;$i++){
	echo "<option value='".$i."'>".$i."</option>";
	}
	?>
    </select>/
	<select name="mes">
	<?php 
	for($i=1;$i<13;$i++){
	echo "<option value='".$i."'>".$i."</option>";
	}
	?>
    </select>/
    <select name="ano">
	<?php 
	for($i=1900;$i<date("Y")+1;$i++){
	echo "<option value='".$i."'>".$i."</option>";
	}
	?>
    </select>
	<br>
	<center><font size="2">dd/mm/aa</font></center>
    </td>
	</tr>

	<tr><td>Seleccione los idiomas a aprender:</td>
	<td>
	<input checked type="checkbox" name="EN" value="1"> Ingl�s <br/>
	<input type="checkbox" name="FR" value="1"> Franc�s <br/>
	<input type="checkbox" name="IT" value="1"> Italiano <br/>
	<input type="checkbox" name="PR" value="1"> Portugu�s <br/>
	<input type="checkbox" name="DE" value="1"> Alem�n <br/>
	<input type="checkbox" name="CH" value="1"> Chino Mandar�n <br/>
	<input type="checkbox" name="JA" value="1"> Japon�s <br/>
	<input type="checkbox" name="KO" value="1"> Coreano <br/>
	<input type="checkbox" name="RU" value="1"> Ruso <br/>
	<!--input type="hidden" name="perf"></td-->
	</tr>

	<tr><td>
	<input type="submit" value="Registrarse" name="boton" class="boton_panel">
	</td>
	</tr></tbody></table>
	</form>
</td>

</html>