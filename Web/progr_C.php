<html>
<head>
<title>Programaci�n en C</title>
<link rel=stylesheet href="estilo.css" type="text/css"> 
   <link href="jquery-ui-ajax-googleapis.css" rel="stylesheet" type="text/css"/>

 <script src="./jquery-1.6.4.js"></script>
 <script src="./jquery-ui.min.js"></script>

  <script>
	function opc(x){	
	document.getElementById("cont").src="res.php?pid="+x;	
		}
	
  	$(function() {
	 $("#secc").accordion({
	 autoHeight: false,
	 navigation: true,
	 collapsible: true,
	 fillSpace: true
	  });
	});
	
  </script>
  
</head>

<body vlink="blue" bgcolor="#787878">

<center>
<div style="width:770; height:10; background-color:#ffffff"></div>
<div style="width:770; height:156; background-image:url(./images/header.jpg); background-repeat:repeat-x">&nbsp; </div>



 <div class="cuerpo" id="c_home">

<script>
	$(function() {
		$( '#tabs' ).tabs();
	});
	</script>
<div class='demo'>

<div id='tabs'>
	<ul>
		<li><a href='#tabs-1'>Instalaci&oacute;n</a></li>
		<li><a href='#tabs-2'>Curso de C</a></li>
		<li><a href='#tabs-3'>Curso de CGI (C)</a></li>
	</ul>
	<div id='tabs-1'>
		<p><p><b>Instalaci�n del entorno</b></p>
		<div align="justify"><br>
		  Lo primero que se debe realizar para desarrollar aplicaciones en C, es descargar y ejecutar el compilador.
		  Hay muchos compiladores de lenguaje C, pero se utilizar� para este curso el gcc.<br>
  <u>Paso 1: Descargar el compilador</u><br>
  <i>Windows:</i><br>
		  Para instalar gcc, hay varios m�todos, pero para efectos pr�cticos se usar� el IDE Dev-Cpp (link de descarga aqui)<br>
  <i>GNU/Linux:</i><br>
		  Ubuntu: Para instalar gcc, se debe ir a Terminal/Konsole (dependiendo del escritorio utilizado), y como super usuario ejecutar:<br>
  <b>sudo apt-get install gcc</b><br>
		  RedHat: Para instalar el compilador, se debe ir a Terminal/Konsole (dependiendo del escritorio utilizado), y como super usuario ejecutar:<br>
  <b>sudo yum install gcc</b><br>
  <br>Se debe utilizar adem�s un editor de texto (usaremos gedit por defecto) 
		  <br>
		  </div>
		</p>
	</div>
<div id='tabs-2'>
  <div id="secc">
  <h3> <a href="#">Primer programa (Hola Mundo)</a></h3>
  <h3 align="justify">El primer programa dentro de cualquier lenguaje de programaci&oacute;n es el cl&aacute;sico &quot;Hola Mundo&quot;, la estructura de cualquier programa de C es:<br>
    <br>
    Cabecera<br>
    Apertura<br>
    Cuerpo<br>
  Cierre<br>
  <br>
  Nuestro primer porgrama en C, se estructura as&iacute;:<br>
  <br>
  #include &lt;stdio.h&gt;<br>
  void main(){<br>
  printf(&quot;Hola mundo desde C&quot;);<br>
  } <br>
  <br>
  Para poder compilar un c&oacute;digo en C bajo GNU/Linux, se debe escribir lo siguiente en la Terminal:<br>
  gcc -o (nombre del programa final) (nombre del c&oacute;digo terminado en .c)<br>
  Y para poder probar el c&oacute;digo compilado se debe ejecutar la ruta dentro de la consola<br>
  La forma de ejecutarlo dentro de Windows es la misma bajo la consola (s&iacute;mbolo de sistema), pero si utiliza un IDE basta con ejecutar dentro de las opciones compilar y luego ejecutar.<br>
  <br>
  <u>NOTA:</u> Algunos compiladores no soportar&aacute;n la opci&oacute;n 'void main', en vez de esto se debe escribir 'int main' y a&ntilde;adir en la &uacute;ltima l&iacute;nea antes del cierre de llave 'return 0'</h3>
  <h3> <a href="#">Aspectos adicionales primarios</a></h3>
  <h3  align="justify">Los primeros aspectos primarias, son las palabras reservadas (1), los argumentos (2), las pausas (3)  y los comentarios(4)<br>
    1) Las palabras reservadas son palabras propias del lenguaje de programaci&oacute;n y no pueden ser utilizadas para otros fines<br>
    2)
    Los argumentos son los datos que se utilizar&aacute;n dentro del programa, &eacute;stos son denotados (para efectos de esta secci&oacute;n) por 2 palabras reservadas argv y argc; argc es el n&uacute;mero de argumentos a utilizar y argv son los nombres de dichos argumentos<br>
    3) Las pausas son detenciones (moment&aacute;neas o finales), dentro del programa
  que servir&aacute;n para una interfaz m&aacute;s amigable frente al usuario, &eacute;stas pueden ser exit, return, break, pause,  o getchar. Para efectos de este curso en primera instancia utilizaremos las 2 &uacute;ltimas.<br>
  4) Con respecto a los comentarios son notas que se pueden incluir dentro de un c&oacute;digo pero que no inciden en su ejecuci&oacute;n<br>
  <br>
  Veamos un ejemplo pr&aacute;ctico para ver lo aprendido:<br>
  <br>
  #include &lt;stdio.h&gt;<br>
  /*&Eacute;sto es un comentario<br>
de varias<br>
l&iacute;neas
*/ <br>
// y &eacute;ste esde una sola l&iacute;nea  <br>
    void main(int argc, char **argv){<br>
    //se incluyeron el ergv y el argc dentro de los par&eacute;ntesis<br>
    printf(&quot;Paises: \n&quot;); <br>
    //N&oacute;tese el '\n' &eacute;sto es un salto de l&iacute;nea<br>
    printf(&quot;%s&quot;, argv[0]); <br>
    //El '\t' es tabulaci&oacute;n, luego se escribir&aacute; el primer argumento
    <br>
     printf(&quot; %s %s&quot;, argv[1], argv[2]);    <br>
     //El resto     <br>
     //fin del programa<br>
     getchar(); <br>
     //espera a presionar una tecla para finalizar <br>
    }<br>
    Hay que tener en cuenta varios aspectos, que DEBEMOS enviarle 3 par&aacute;metros
  al programa para que funcione &iquest;Como? el la terminal/prompt escribir luego de compilarlo<br>
  (nombre del programa compilado) (argumento1) (Argumento 2) (Argumento 3)<br>
  Ejemplo: ./progr Chile Uruguay Argentina
  </h3>
<h3> <a href="#">Variables</a></h3>
  <h3  align="justify">Lo escencial para aprender a programar es saber los tipos de datos, los siguientes son una vision general:<br>
Entero (int); Vale decir cualquier n&uacute;mero entero (positivo o negativo)<br>
Flotante (float); O sea numeros decimales (con punto flotante de ah� el nombre)<br>
Caracter (char); es cualquier s&iacute;mbolo alfanum&eacute;rico (n&uacute;meros o letras)<br>
String (string); es cualquier cadena de caracteres, o sea, palabras que pueden contener letras y n&uacute;meros<br>
Hay otro tipo de datos como:<br>
Double (double); son flotantes con una precisi&oacute;n mayor de decimales<br>
Booleano (bool); es un tipo de datos con 2 valores; verdadero o falso<br>
As&iacute; como 'prefijos' a los tipos de datos.<br>
unsigned; utilizado para los tipos de datos num&eacute;ricos sirve para restringir el rango de n&uacute;meros posible a solo positivos<br>
short, delimita el rango de datos num&eacute;rico<br>
long, ampl�a el rango de datos num&eacute;rico<br>

Ahora un poco de c&oacute;digo:<br>
#include &lt;stdio.h&gt;<br>
void main(){<br>
int num = 5;      // Esta variable num tiene un valor de 5<br>
int num2;    // Esto tambi�n es una variable<br>
num2 = 4;  //  declarada en 2 l�neas<br>
char letra = 'c' //Esta varable es un caracter simple, se declaran siempre entre '<br>
float e = 2.713; // Este es un numero flotante<br>
char palabra[] = "hola"; /* Esta es una palabra (String) siempre se declaran entre &quot; */<br>
char palabra2[] ="12345"; //Esto es un String de n&uacute;meros<br>
unsigned int otro_num = 5; //Este es un entero sin signo<br>
//Ahora vamos a escribir variables<br>
//para escribir enteros<br>
printf("La variable num es :%d\n", num);
// O tambien podemos escribir 2 variables<br>
printf("La variable num es: %d y la segunda es: %d\n", num, num2);
// podemos ejecutar operaciones num&eacute;ricas<br>
printf("La suma de ambas es: %d y la resta es %d", num+num2, num-num2);
//podemos escribir flotantes<br>
printf("El flotante es %f\n", e);
// tambien podemos cambiar la precisi&oacute;n del numero<br>
printf("La variable reducida es %0.2f\n");
// Hay que aclarar que no podemos sumar la variable palabra2 con cualquier<br>
// entero, ya que son tipos de variables diferentes<br>
//ahora, un &uacute;ltimo truco al escribir lo siguiente<br>
printf("La variable %c no es vac�a\n", letra);<br>
// n&oacute;tese que hay un acento, &iquest;c&oacute;mo podemos escribir con acento?<br>
/* Con el c�digo ASCII, en este caso lo usaremos para resolver esto
el c&oacute;digo ASCII es, a grandes rasgos, un tipo de codificaci�n de
 caracteres
determinado por una tabla, �De qu� nos sirve?
intentemos lo siguiente */<br>
printf("La variable %c no es vac%ca\n", letra, 237");<br>
//La letra '�' es el equivalente al c�digo 237<br>
/* Por �ltimo, para imprimir cadenas completas de caracter se escriben de la siguiente manera */<br>
printf("Para escribir palabras se escribe %s", palabra);<br>

getchar();<br>

}<br>
Hay algunos caracteres como '\n', '\t', '\a' o '\b', &Eacute;stos determinan distintos comportamientos en la l�nea printf<br>

  <table border="2">
      <tr>
        <td><h3><strong>Simbolo</strong></h3></td>
        <td><h3><strong>Significado</strong></h3></td>
      </tr>
      <tr>
        <td><h3>\n</h3></td>
        <td><h3>Salto de linea</h3></td>
      </tr>
      <tr>
        <td><h3>\t</h3></td>
        <td><h3>Tabulaci&oacute;n horizontal</h3></td>
      </tr>
      <tr>
        <td><h3>\a</h3></td>
        <td><h3>Campana de alerta</h3></td>
      </tr>
      <tr>
        <td><h3>\b</h3></td>
        <td><h3>Caracter de retroceso (backspace) </h3></td>
      </tr>
      <tr>
        <td><h3>\\</h3></td>
        <td><h3>Slash</h3></td>
      </tr>
      <tr>
        <td><h3>\'</h3></td>
        <td><h3>Comilla simple</h3></td>
      </tr>
      <tr>
        <td><h3>\"</h3></td>
        <td><h3>Comilla doble</h3></td>
      </tr>
  </table>
  <br>
    Una aclaraci&oacute;n acerca del nombre de las variables:<br>
  1) No pueden iniciar con n&uacute;meros<br>
  2) No pueden contener caracteres especiales (&uuml;,&oacute;, *, etc)<br>
  3) No pueden contener espacios<br>
  4)   No pueden ser palabras reservadas (palabras propias pertenecientes al lenguaje C como printf, sacnf, void, main, return, etc.)<br>
  </h3>
  <h3> <a href="#">Escaneo de Variables</a></h3>
  <h3  align="justify">Ahora que sabemos manipular variables, pero hasta el momento s�lo podemos utilizar unas dadas por el programa anteriormente, ahora vamos a poder determinar por nosotros mnismos las variables:<br>
#include &lt;stdio.h&gt;<br>
void main(){<br>
int var 5;<br>
printf("Escriba una variable: ");<br>
scanf("%d",&var);<br>
//Desglosamos esta instrucci�n m�s abajo
<br>
printf("Asi que tu numero es %d\n", var);
<br>
}<br>

La instrucci�n 'scanf', extrae variables por el teclado
<br>
scanf("%d",&var);
<br>
- Es el tipo de variable que vamos a escanear, de la misma manera que escribir�amos en printf<br>
es decir, si escnearamos un flotante tengr�amos que escribir '"%f"', un caracter "'%c'" o un string '"%s"'<br>
scanf("%d",<u>& </u>var);<br>
- El caracter amperson (&), representa en este caso un direccionamiento de memoria, es decir, lo que le digo al compilador es "direcciona lo que acabo de escribir a la variable..."<br>
scanf("%d",<u>&var </u>);<br>
- Es la variable a la cual direcciono la instrucci�n</h3>
 <h3> <a href="#">Operadores Aritm&eacute;ticos</a></h3>
  <h3 align="justify">Los mutadores cambian el valor de las variables (suman, restan, multiplican, etc), veamos con un ejemplo<br>
#include &lt;stdio.h&gt;<br>
void main(){<br>
int x, y;<br>
printf("Escribe un n�mero:\t");<br>
scanf("%d", &x);<br>
printf("Escribe otro n�mero:\t");<br>
scanf("%d", &y);<br>
printf("La suma de %d con %d es: %d", x, y, x+y);<br>
printf("La resta de %d con %d es: %d", x, y, x-y);<br>
printf("La multiplicaci�n de %d con %d es: %d", x, y, x*y);<br>
printf("La suma de %d con %d es: %f", x, y, x/y);<br>
//se pone %f, ya que si fuese %d, escribir�a un entero sin punto <br>
//ejemplo: si fuesen 5 y 2; con %f = 2,5, si fuese con %d = 2<br>
printf("El m�dulo de %d con %d es: %f", x, y, x%y);<br>
//Un m�dulo es el resto de dividir 2 n�meros<br>
//Ahora vamos a cambiar las varaiables<br>
x = x+1; //Hemos aumentado en 1 la variable<br>
y = y*6; //Hemos multiplicado la variable x6<br>
printf("La suma de %d con %d es: %d", x, y, x+y);<br>
x=x-4; //Hemos disminuido la variable en 4<br>
y= y/2 // Hemos dividido a la mitad la variable<br>
x= x%2 // Hemos cambiado la variable a su resto al dividirlo por 2
<br>
//Hay otras formas de escribir lo anterior como por ejemplo<br>
// x+=1 ::::: y*=6 ::::: x-=4 :::::  x%=2<br>
// Adem�s hay otra forma de aumentar o disminuir en uno (espec�ficamente)<br>
// x++; aumentar ::::::::::::::: y--; disminuir<br>
getchar();<br>
}</h3>
<h3><a href="#">Operdaores Relacionales</a></h3>
<h3 align="justify">Los principales son:<br>
  Asignaci�n&nbsp;&nbsp;&nbsp;&nbsp;=<br>	
  Mayor/Menor que &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&gt;   &lt;<br>
  Mayor/Menor o igual que&nbsp;&nbsp;&nbsp;&nbsp; &gt;=   =&lt;<br>
  Igual que &nbsp;&nbsp;&nbsp;&nbsp;==<br>
  Distinto de &nbsp;&nbsp;&nbsp;&nbsp;!=<br>
  Veamos el c�digo:<br>
  #include &lt;stdio.h&gt;<br>
  void main(){<br>
  int num;<br>
  scanf("%d",&num);<br>
  if(x > 0)<br>
  printf("El numero es mayor que cero");<br>
  if(x >= 0)<br>
  printf("El numero es mayor o igual que cero");<br>
  if(x < 0)<br>
  printf("El numero es menor que cero");<br>
  if(x <= 0)<br>
  printf("El numero es menor o igual que cero");<br>
  if(x == 0)<br>
  printf("El numero es igual a cero");<br>
  if(x != 0)<br>
  printf("El numero no es igual a cero");<br>
  }<br>
  
  Existen otros operadores pertenecientes a otro tipo de como<br>
  === &nbsp;&nbsp;&nbsp;&nbsp; Equivalencia de valor y de tipo de dato <br>
  .equals(); &nbsp;&nbsp;&nbsp;&nbsp; Equivalencia de objetos o Strings 
</h3>
<h3><a href="#">Operdaores L&oacute;gicos</a></h3>
<h3 align="justify">Erte tipo de operadores son los que determinan 2 resultados posibles; Verdadero o Falso.<br>
B&aacute;sicamente son 4 operadores:<br>
<table border="2" width="100%"><tr>
<td><h3>&amp;&amp;&nbsp;&nbsp; AND</h3></td>
<td><h3>||&nbsp;&nbsp; OR</h3></td>
<td><h3> !&nbsp;&nbsp;NOT(Negaci&oacute;m)</h3></td>
</tr></table><br>
Existe otro operador que es el XOR (OR exclusivo), que no se presenta en el lenguage C
Veamos un c�digo:<br>
#include&lt;stdio.h&gt;<br>
void main(){<br>
int x;<br>
printf("Escribe un n&uacute;mero");<br>
scanf("%d",&amp;x);<br>
if(x&gt;0)&amp;&amp;(x&lt;5)<br>
printf("El numero es mayor que cero Y menor que 5");<br>
if(x&gt;0)||(x&lt;0)<br>
printf("El numero PUEDE ser mayor que cero O PUEDE ser menor que 0");<br>
if(!(x==0))<br>
printf("El numero no es igual a 0");<br>
/* tambi�n puede realizarse con != pero lo anterior es netamente para demostrar el ejemplo */<br>
getchar();<br>
}
</h3>
<h3><a href="#">Secuencias de Control</a></h3>
<h3 align="justify">Las secuencias de control son b�sicamente condiciones que se cumplen bajo ciertas condiciones, son palabras reservadas en muchos lenguajes:<br>
<ul>
<li>if / if...else
<li>switch case
<li>for
<li>foreach (no se encuentra en C)
<li>while / do while
</ul>
Ya hemos visto como primer acercamiento el IF en los ejemplos anteriores, veamos un c&oacute;digo:<br>
# include &lt;stdio.h&gt;<br>
void main(){<br>
int x;<br>
scanf(&quot;%d&quot;, &amp;x);<br>
if(x==0){<br>
printf(&quot;x es 0&quot;);<br>
}<br>
else{<br> 
printf(&quot;x no es cero&quot;);<br>
}<br>
//lo anterior es bstante explicativo si ocurre algo o no ocurre pueden ser varios if y luego un else final<br>
/*es importante destacar que las secuencias de control pueden estar con par&eacute;ntesis
<br>
de llave o no, cuando lleven estos par&eacute;ntesis pueden incluir m&aacute;s de una l&iacute;nea, cuando es el caso contrario
s&oacute;lo se ejecutar&aacute; la linea siguientes */<br>
for(x=0; x&lt;5; x++)<br>
printf(&quot;%d&quot;, x);<br>
/*un ciclo for es netamente un contador, que en este caso,
se incrementar&aacute; el valor de x de 1 en 1*/<br>
for(x=0;x&lt;10;x+=2){<br>
printf(&quot;%d &quot;,x);<br>
 printf(&quot;%d &quot;,x+1);<br>
}//en este caso se aumenta en 2, puede disminuir o ir multiplicando etc. y lleva par&eacute;ntesis de llave (deliberadamente)<br>
//Ahora veamos un ciclo while<br>
while(x != 0){<br>
printf("%d", x);<br>
x-=2;<br>
}//mientras el n�mero no sea 0 se ira dismuyendo en 2 hasta ser 0, recordemos que x era igual a 10<br>
//la diferencia con un do-while, es que &eacute;ste &uacute;ltimo se ejecutar&aacute; al menos una vez, el while puede no ejecutarse<br>}<br>
un switch son una serie de opciones distintas (como un men&uacute;), tiene la siguiente estructura<br>
switch(opcion){<br>
case opc1:<br>
break; <br>
case opc2:<br>
break; <br>
default:<br>
}<br>
Ejemplo:<br>
scanf(&quot;%d&quot;&amp;opc);<br>
switch(opc){<br>
case 1:<br>
printf(&quot;Seleccionada opci&oacute;n1&quot;); <br>
case 2:<br>
printf(&quot;Seleccionada opci&oacute;n2&quot;); <br>
default:<br>
printf(&quot;Ha escrito cualquier n&uacute;mero excepto 1 o 2&quot;); <br>
} <br>
</h3>
<h3><a href="#">Arreglos y Strings</a></h3>
<h3 align="justify">Si tenemos varias variables que tienen algo en com�n, por ejemplo, la temperatura de los d�as de una semana es f�cil escribirlos (int lu, ma, mi, ju ,vi sa, do;) pero &iquest;Que pasa cuando queremos lo mismo pero de un a&ntilde;o? No vamos a escribir int t1, t2, t3, .... t365<br>
Para eso est&aacute;n los arreglos, &eacute;stos se escriben:<br>
(tipo de variable) nombre_variable[numero];<br>
para el caso anterior ser&iacute;an<br>
float temperatura_anho[365];<br>
Se declaran y se determinan (usando el ejemplo de la semana) de dos maneras:<br>
Uno a uno :<br>
int semana[5];<br>
semana[0] = 31;<br>
semana[1] = 24;<br>
semana[2] = 29;<br>
semana[3] = 40;<br>
semana[4] = 34;<br>
Todo en una l&iacute;nea:<br>
int semana[5]= {31,24,29,40,34};<br>
Se denotan 2 aspectos, primero TODO primer elemento tiene como &iacute;ndice 0 y el �ltimo &iacute;ndice nunca se ocupa, es decir, si es un arreglo de 5 elementos el &iacute;ndice 5 nunca se utiliza; segundocuando se declaran dentro de una l&iacute;nea, el &iacute;ndice se puede obviar (puede no siempre escribirse)<br>
Los strings son un tipo de arreglo especial (en C), son s&oacute;lo de caracteres , la forma de escribirlos y escanearlos, ya se vi&oacute; en secciones anteriores, pero hay que resaltar que hay una librer&iacute;a especializada para strings en c &lt;string.h&gt;
</h3>
<h3><a href="#">Funciones y Procedimientos</a></h3>
<h3 align="justify">En C el programa principal viene determinado por la funci&oacute;n <u>main</u>, las funciones son fundamentales para la ceaci&oacute;n de c&oacute;digos, por una parte un buen programador debe tener todo separado en funciones (el main no debe superar las 10 l&iacute;neas), viene de la frase 'divide y vencer&aacute;s', las funciones tienen las siguientes reglas<br>
<ol><li>Pueden (o no) tener variables de entrada de cualquier tipo
<li>Deben tener par&aacute;metros de salida, cuando no los haya se llamar&aacute;n procedimientos o m&eacute;todos
</ol>
En C, las funciones tienen la misma estructura que en el main (es una funci&oacute;n despu&eacute;s de todo):<br>
(tipo de variable resultado) nombre_de_la_funci&oacute;n (variables de entrada), se escriben antes del main<br>
Por ejemplo, simulemos la potencia de un n&uacute;mero, esto vendr&aacute; definido por la base y el exponente<br>
<blockquote>float potencia(float base, int exp){<br>
float total = 1;<br>
count = 1;<br>
if(exp == 0){<br>
 &nbsp;&nbsp;&nbsp;if(base ==0){<br>
  &nbsp;&nbsp;&nbsp;printf(&quot;Error&quot;);<br>
  &nbsp;&nbsp;&nbsp;return 0;<br>
  &nbsp;&nbsp;&nbsp;}<br>
  &nbsp;&nbsp;&nbsp;else<br>
  &nbsp;&nbsp;&nbsp;return 1;<br>
} <br>
else<br>
whlie(count != exp){<br>
total *=base;<br>
count++;<br>
}<br>
return total;<br>
}
</blockquote>
void main(){<br>
float  x= 2.1;<br>
int y= 3;<br>
printf(&quot;%f elevado a %d es: %f&quot;, x, y, potencia(x,y));<br>
}<br>
Declaramos la funci&oacute;n vemos que en el main pueden ser otros los nombres de las variables, en la funci&oacute;n son 'base' y 'exponente' y en main son 'x' e 'y', pero DEBEN ser el mismo tipo de variables, si vemos el valor que retorna la funci&oacute;n es un flotante<br>
Los procedimientos o m&eacute;todos son funciones que no nretornan ning&uacute;n valor, veamos un procedimiento que muestra el promedio de x n&uacute;meros, y que no tenga par&aacute;metros<br>
<blockquote> void promedio(){<br>
float total=0, num, cont=0;<br>
char agregar;
do(<br>
scanf("%d",&amp;num);<br>
total += num;
cont ++;<br>
printf(&quot;Desea agregar otro numero?(s\\n)&quot;);<br>
scanf(&quot;%c&quot;, agregar);<br>
}while(agregar =='s');<br>
printf("El promedio es %f", total/cont);<br>
}</blockquote>
void main()}{<br>
promedio();<br>
}<br>
El procedimiento escribe el n&uacute;mero, sin necesidad de ning&uacute;n par&aacute;metro
</h3>
<h3><a href="#">Prototipos y Recursividad</a></h3>
<h3 align="justify">Utilizaremos la  funci&oacute;n potencia, para ejemplificar ambos conceptos, el prototipo de una funci&oacute;n es declarar la funci&oacute;n para despu&eacute;s ser utilizada (similar a lo hecho con las variables), si bien las funciones y procedimients pueden ser escritas al principio de main, &eacute;sto resulta poco est&eacute;tico, primero se declaran los prototipos, luego se escriben al final de main:<br>
  <blockquote>float potencia(float, int);<br>
  main(){...}<br>
  float potencia(float base, int exponente){...}</blockquote>
  As&iacute;, las funciones se escribir&aacute;n tal cual, pero al declarar sus prototipos, las variables de entrada s&oacute;lo
  son declaradas por el tipo, &eacute;stas deben concordar en el orden<br>
  Recursividad ocurre cuando una funci&oacute;n est&aacute; inserta dentro de s&iacute; misma, ejemplo:<br>
  int nombre_funci&oacute;n(par&aacute;metros){<br>
  	int x= nombre_funci&oacute;n(par&aacute;metros);<br>
  } <br>
  Aplicado a la funci&oacute;n potencia, tenemos que funciona para cualquier exponente positivo o igual a acero &iquest;y si el exponente fuera negativo? Hay que corregir la funci&oacute;n
  <br><blockquote>float potencia(float base, int exp){<br>
float total = 1;<br>
count = 1;<br>
if(exp == 0){<br>
 &nbsp;&nbsp;&nbsp;if(base ==0){<br>
  &nbsp;&nbsp;&nbsp;printf(&quot;Error&quot;);<br>
  &nbsp;&nbsp;&nbsp;return 0;<br>
  &nbsp;&nbsp;&nbsp;}<br>
  &nbsp;&nbsp;&nbsp;else<br>
  &nbsp;&nbsp;&nbsp;return 1;<br>
} <br>
else{<br>
if(exp &gt; 0){
<br>
whlie(count != exp){<br>
total *= base;<br>
count++;<br>
}<br>
return total;<br>
}<br>
if(exp &lt; 0)<br>
&nbsp;&nbsp;&nbsp;total = (1/potencia(base,(exp * -1)));<br>
} </blockquote>
  De esa forma si el exponente es negativo, se realizar&aacute; la misma operaci&oacute;n que con un positivo (se cambia el par&aacute;metro para volverlo positivo y el resultado se invierte)</h3>
<h3><a href="#">Alcance de Variables (Locales y Globales)</a></h3>
<h3 align="justify">&iquest;Hasta donde se pueden usar variables de entrada? Est�n determinados por su localidad o globalidad<br>
O sea, una variable puede ser global o local, veamos un ejemplo:<br>
<blockquote>#include &lt;stdio.h&gt;<br>
int a = 5;<br>
void prod(int a){<br>
    printf("%d ", a);<br>
    }<br>
void cinco_por(float b){<br>
     printf("%0.2f", a * b);<br>
     }<br>
void main(){<br>
    prod(a);<br>
    int a=4;<br>
    prod(a);<br>
    cinco_por(0.5);<br>
    getchar();<br>
    }</blockquote>
 Tenemos una a global(=5) y una local en el main(=4), el procedimiento prod escribe la variable que se le env&iacute;a, en primera instancia escribe 5 (ya que recibe la 'a global'), luego el mismo procedimiento escribe 4 (recibe la 'a local'), sin embargo al escribir el procedimeinto cinco_por vuelve a recibir el 'a global', devolver&aacute; '2.50'.</h3>
<h3><a href="#">Punteros</a></h3>
<h3 align="justify">Un puntero es un espacio de memoria que est&aacute; direccionado a cualquier otro espacio de memoria (puden ser variables, funciones, archivos, otros punteros), un puntero se escribe de la siguiente manera:<br>
tipo_de_variable *nombre_variable;<br>
Ejemplo:<br>
int a[4];<br>
int *b= a;<br>
printf("%d, %d, %d", &a, &b, b);<br>
Escribir&aacute; 3 direcciones de memoria b toma el valor de la direcci&oacute;n de memoria de a (a&uacute;n incluso siendo a un arreglo de enteros) los punteros son fundamentales para la programaci&oacute;n de estructuras de datos<br>
Los punteros por ser espacios de memoria que se reservan, se deben saber otras funciones referentes a los espacios de memoria, por ahora solo veremos; 'malloc', 'free',  'sizeof' y typedef<br>
sizeof(varaiable): Es una palabra reservada que determina el n&uacute;mero de bytes que utiliza una variable (pasada por par&aacute;metro)<br>
Agregando: printf(&quot;%d, %d, %d, %d&quot;, sizeof(&amp;a), sizeof(&amp;b), sizeof(b), sizeof(a));<br>
Se ven cuantos bytes reservan para cada puntero, como es de esperar cada uno reservar&aacute; 4 bytes a&uacute;n cuando apunte a un arreglo de enteros<br>
malloc(int): Es una funci&oacute;n que reserva de antemano un n&uacute;mero determinado de bytes, es decir, para malloc(20), se reservan 20 bytes, puede devolver un puntero a NULL o al espacio asignado <br>
free(*puntero): libera la memoria reservada por un puntero pasado como variable <br>
typedef: no esprecisamente una funci&oacute;n para punteros sirve para dar un alias a un tipo de  variable determinada, es com&uacute;nmente utilizada para estructuras de datos. Se utiliza de la siguiente manera:<br>
typedef (tipo de variable) nombre_alias_de_variable;<br>
typedef unsigned int entero_positivo;<br>
Luego estos nuevos tipos de variable se pueden declarar e igualar como cualquier otro:<br>
entero_positivo x = 4;
</h3>
<h3><a href="#">Paso de variables por valor y por referencia</a></h3>
<h3 align="justify">Ya vimos el paso de variables, pero ahora podemos verlo desde un punto de vista distinto, el paso de variables ya visto es el paso por valor, es decir, se guarda lo que se retorna a una variable:<br>
float cuadr(float x){<br>
return (x*x);<br>
}<br>
Ahora el resultado puede ser referenciado por un puntero:<br>
float cuadr2(float *x){<br>
return ((*x) * (*x));<br>
}<br>
void main(){<br>
float x = 2.5<br>
printf(&quot;%0.2f&quot;, cuadr2(&amp;x);<br>
} <br>
La forma de pasar variables, implica un coste de memoria menor al realizarlo por referencia, si se tratara de un arreglo de enteros habr&iacute;a que dar una variable de x enteros a diferencia de un puntero que es un espacio de memoria menor</h3>
<h3><a href="#">Estructuras</a></h3>
<h3 align="justify">Las estructuras de datos son el primer paso para la programaci&oacute;n avanzada de cualquier lenguaje de programaci&oacute;n, una estructura b&aacute;sicamente es un tipo de dato dise&ntilde;ado para diversos prop&oacute;sitos, como cualquier tipo de dato se puede declarar y/o guardar, desde el punto de vista son colecciones de diversos tipos de datos<br>
  Para declarar una estructura cualquiera se debe escribir antes del main<br>
  struct (nombre de la estructura) {<br>
  (_declaraciones_de_datos_);<br>
  };<br>
  Ejemplo:<br>
  struct persona{<br>
  char  rut[10];<br>
  char nombre[30];<br>
  char apellido[30];<br>
  int edad;<br>
  }<br>
Si bien la estructura est&aacute; definida para iniciar una estructura se puede realizar de 2 maneras:<br>
<ol>
  <li>Agregando al final de la estructura un nombre:<br>
    struct persona{<br>
    ...<br>
    }pers;
    <br>
    Incluso se pueden realizar un arreglo de estructuras agregando a pers un &iacute;ndice
    <li>Declar&aacute;ndola dentro del main:<br>
      void main (){<br>
      struct persona pers;<br>
    }</ol>
El hecho de que una estructura sea declarada no necesariamente se deben tener todos los registros con variables el main puede ser perfectamente:<br>
void main(){<br>
struct persona pers;<br>
scanf(&quot;%d&quot;, &amp;pers.edad):<br>
scanf(&quot;%s&quot;, pers.nombre);<br>
printf(&quot;%s tiene %d a%cos\n&quot;, pers.nombre, pers.edad, 164);<br>
getchar();getchar();<br>
    }<br> 
    Las estructuras anidadas son estructuras dentro de otras estructuras, vale decir:<br>
    struct empleado{<br>
    int id_empleado;<br>
    char cargo[30];<br>
    struct persona pers;<br>
    }<br>
    En este caso la estructura empleado tiene impl&iacute;cita la estructura persona, para acceder a sus datos seria con:<br>
    struct empleado e;<br>
    printf(&quot;%s tiene %d a%cos, tiene el cargo de %s con un id de la empresa de %d\n&quot;, e.pers.nombre, e.pers.edad, 164, e.cargo, e.id_empleado);
</h3>
<h3><a href="#">Uso de Archivos</a></h3>
<h3 align="justify">Ya sabemos como escribir y extraer varibles desde el teclado, pero los datos se pierden al apagar el el equipo, aqu&iacute; intervienen el uso de archivos, para manipular un archivo (en el lenguaje que sea) )se deben realizar 3 pasos en este orden:
  <ol><li> Se debe abrir un archivo
<li> Se ejecutan operaciones, de entrada o salida de datos
<li> Se cierra el flujo de datos al archivo</ol>
Ahora vamos a ver un c&oacute;digo par manipular archivos:<br> 
#include &lt;stdio.h&gt;<br>
void main(){<br>
//Vamos a abrir un archivo, para eso se debe crear un puntero especial<br>
FILE *arch; // debe ser en may&uacute;sculas 'FILE', el nombre 'arch' puede ser cualquiera<br>
/*(1) Vamos a abrir un archivo, un archivo puede ser abierto de 3 formas; escritura, lectura, modo ap�ndice, adicionalmente se pueden abrir de 2 formas; modo texto (por defecto) o binario*/<br>
/*Para abrir un  archivo se utiliza la palabra reservada fopen, que debe tener 2 atributos, el archivo en cuesti&oacute;n y la forma de apertura<br>
Formas de  abrir un  archivo:<br>
<table width="331" border=2>
<tr><td width="66"><h3>C&oacute;digo</h3></td><td width="247"><h3>Significado</h3></td></tr>
<tr><td><h3>r</h3></td><td><h3>lectura</h3></td></tr>
<tr><td><h3>w</h3></td><td><h3>escritura</h3></td></tr>
<tr><td><h3>a</h3></td>
<td><h3>ap&eacute;ndice</h3></td></tr>
<tr><td><h3>r+</h3></td>
<td><h3>lectura, se crea el archivo si no existe</h3></td></tr>
<tr><td><h3>w+</h3></td>
  <td><h3>escritura, se  reemplaza el archivo</h3></td></tr>
<tr><td><h3>a+</h3></td>
  <td><h3>ap&eacute;ndice,  se crea el archivo si no existe</h3></td></tr>
</table>
*/ 
<br>
  arch = fopen(&quot;datos.txt&quot;, &quot;w+&quot;);<br> 
  //Hemos creado un archivo se reemplaza si es que no existe<br>
  //Ahora escribieremos una l&iacute;nea dentro del archivo, aunque hay varias funciones que realizan la misma funci&oacute;n<br> 
  /*Entre otras est&aacute;n fputs, fwrite, fprintf*/<br>
  fprintf(arch, &quot;Hola :)&quot;);<br>
  //(2) Escribimos 'Hola :)' dentro de un archivo datos.txt<br>
  fclose(arch);<br>
  //(3) Se cierra el flujo de archivos con la funci&oacute;n fclose(), que recibe como argumento el flujo<br>
}<br>
  Para extraer informaci&oacute;n desde un archivo tambi&eacute;n hay varias funciones (fgets, fgetc, fscanf, fread)<br>
Depende de lo que desee realizar, en este caso s�lo extraeremos una l&iacute;nea, para eso luego de abrir un archivo:<br>
char linea[200];<br>
arch2 = fopen(&quot;datos.txt&quot;. &quot;r+&quot;);<br> 
fscanf(arch2, %s, linea);<br>
De esa manera podemos ingresar y extraer datos a un archivo
</h3>
<h3><a href="#">TAD y Listas</a></h3>
<h3 align="justify">Los TAD es el acr&oacute;nimo de Tipo Abstracto de Datos. Son estructuras avanzadas, un ejemplo de ellas son las Listas.<br>
  Imaginemos que tenemos un arreglo de 100 enteros (digamos int datos[100];), cada entro almecena un id de persona, &iquest;Qu&eacute; pasa cuando usamos menos de 100 registros? &iquest;o usamos m&aacute;s de 100? Despreciamos el espacio o agregamos de a 100 registros, ahora imag&iacute;nese la situaci&oacute;n por 100.000 o 10.000.000, o incluso si no se sabe cu&aacute;ntos registros se agregar&aacute;n,es decir, los registros son de largo din&aacute;mico, no parece &uacute;til que se desperdicie tal cantidad de memoria; para eso se utilizan las Listas<br>
Las listas son estructuras de largo din�mico, se componen normalmente de nodos (registros), las operciones b&aacute;sicas de las listas son: buscar, insertar, eliminar y modificar.<br>
Veamos un c&oacute;digo:<br>
# include &lt;stdio.h&gt;<br>
//Para declarar una TAD, se requiere de nodos -que por lo general tienen un dato-, &eacute;stos son de la siguiente forma<br> 
struct nodo{<br>
int dato;<br>
struct nodo *sig;<br>
} <br>
<br>
void insertar(struct nodo **L);<br>
void listar(struct nodo *L);<br>
void modificar(struct nodo *L, int nodo);<br>
void borrar(struct nodo **L);<br>

void main(){<br>
int salida = 0, opc, nro_reg = 0;<br>

struct nodo *L;<br>
L = NULL;<br>
<br>
do{<br>
printf("1) Insertar\n2) Borrar\n3) Modificar\n4) Listar\n5) Salir\n=============================\n");<br>
    printf("Seleccione una opcion:\t");<br>
    scanf("%d",&opc);<br>
    switch(opc){<br>
                case 1:<br>                  
                     insertar(&L);<br>
                     nro_reg++;<br>
                     break;<br>
                case 2:<br>
                     borrar(&L);<br>
                     nro_reg--;<br>
                     break;<br>
                case 3:<br>
                     modificar(L, nro_reg);<br>
                     break;<br>
                case 4:<br>
                     listar(L);<br>
                     break;<br>
                case 5:<br>
                     salida = 1;<br>
                     break;<br>
                default:<br>
                     salida = 0;<br>
                        break;<br>
                }<br>
    }while(salida == 0);<br>

getchar();  <br>
    }<br>

void insertar(struct nodo **L){<br>
     int ndato;<br>
     struct nodo *p, *aux, *lista ;<br>
     p = (struct nodo *) malloc(sizeof(struct nodo));<br>
     printf("Ingrese un nuevo n�mero:\t");<br>
     scanf("%d",&ndato);<br>
     p->dato = ndato;<br>
     
     p-> sig = NULL;<br>
     aux = NULL;<br>
     lista = *L;<br>
     <br>
     while(lista != NULL){<br>
                 aux = lista;<br>
                 lista = lista->sig;<br>
                 }<br>
     if(aux == NULL){<br>
            p->sig = *L;<br>
            *L = p;<br>
            }<br>
     else{<br>
          aux->sig = p;<br>
          p->sig = lista;<br>
          }<br>
     }<br>
<br>
void listar(struct nodo *L){<br>
     if(L != NULL){<br>
     printf("%d,  ", L->dato);<br>
     listar(L->sig);<br>
     }<br>
     else<br>
     printf("\n");<br>
     }<br>
<br>     
void modificar(struct nodo *L, int nodo){<br>
     int pos, i;<br>
     do{<br>
     printf("Seleccione la posici�n del nodo a modificar:\t");<br>
     scanf("%d", &pos);<br>
     }while((pos > nodo) || (pos < 1));<br>
     i=0;<br>
     if(L == NULL)<br>
          printf("No hay datos en la lista\n");<br>
     else<br>
     while((L != NULL) && (i < (pos-1))){<br>
          i++;<br>
          L = L->sig;<br>
          }<br>
          <br>
          printf("Escriba el dato modificado:\t");<br>
          scanf("%d", &pos);<br>
          L->dato = pos;<br>
     }<br>
<br>     
void borrar(struct nodo **L){<br>
     struct nodo *nodo_ant, *nodo_rec, *nodo_aux;<br>
     <br>
     if(*L == NULL)<br>
          printf("No hay datos en la lista\n");<br>
     else<br>
     {<br>
     <br>
     int pos, i;<br>
     printf("Seleccione la posici�n del nodo a borrar:\t");<br>
     scanf("%d", &pos);<br>
     i=0;<br>
     <br>
     nodo_rec = *L;<br>
     <br>
     if(i == (pos-1)){<br>
         nodo_aux = *L;<br>
         *L = (*L)->sig;<br>
         free(nodo_aux);<br>
          }<br>
     else<br>
     while ( (nodo_rec != NULL) && (i != (pos-1)) ){<br>
              i++;<br>
              nodo_ant = nodo_rec;<br>
              nodo_rec = nodo_rec->sig;<br>
              }<br>
          if(nodo_rec != NULL){<br>
          nodo_aux = nodo_rec;<br>
          nodo_ant->sig = nodo_rec->sig;<br>
          free(nodo_aux);<br>
          }<br>
     }}<br>

     <br>
     Para agregar un nodo a una lista enlazada se deben realizar los siguientes pasos:<br>
     <ol><li>Crear un espacio de memoria y definirlo
     <li>Enlazarlo a la lista</ol>
     Se reserva un nodo de memoria usando un puntero y se le definen los datos (en este caso es un entero, pero puede ser una estructura), luego est&aacute; para enlazarlo se usa un auxiliar de 'pivote'; primero se se recorre la lista hasta el �ltimo valor nulo, si no tiene datos esto no ocurre de esta manera el valor auxiliar ser�a nulo tambi�n e insertar� el primer dato, si no, se agregar� al ultimo valor, la forma de insersi�n puede ser agregados de forma ordenada, al principio de la lista, etc.<br>
     Para borrar un registro, se procede a recorrer linealmente la lista, hasta encontrar el registro que se desea borrar (en este caso se borra de acuerdo a la posici�n, aunque tambi�n puede ser en funci�n del valor del nodo)
</h3>
 <h3><a href="#">Listas doblemente enlazadas y circulares</a></h3>
<h3 align="justify">Las listas doblemente enlazadas son aquellas que adem�s de tener un puntero al siguiente valor, tienen un puntero al valor anterior, para el primer elemento �ste apuntar� a NULL<br>
struct nodo{<br>
int valor;<br>
struct nodo *sig;<br>
struct nodo *ant;<br>
}<br>

Para los casos de inserci�n y borrado, s�lo se a�ade las referencias necesarias adicionales, es decir, para a�adir un elemento, hay que realustar n� solo el puntero a siguiente sino que tambi�n al anterior, lo mismo ocurre para el caso del borrado de elementos; donde se deben eliminar ambas referencias para luego liberar la memoria<br>
El caso de las listas circulares son una variaci�n, los datos no tienen una ubicaci�n espec�fica ya que, el puntero del �ltimo elemento apunta hacia el primer elemento, y el puntero anterior del primer elemento apunta hacia el �ltimo elemento
</h3>
<h3><a href="#">Pilas y Colas</a></h3>
<h3 align="justify">Las colas son un tipo de lista espec�fico que son del estilo FIFO (First In First Out), es decir, el primer elemento ingresado es el primero que sale de la lista, los nodos son de la siguiente estructura:<br>
struct nodo{<br>
int valor;<br>
struct nodo *sig;<br>
struct nodo *primer_nodo;<br>
}<br>
Los nodos apuntan al siguiente y al primer elemento, las colas tienen la particularidad de que los valores van agreg�ndose al final de la lista y van borr�ndose al principio de la misma<br>
Para el caso de las pilas son una lista del tipo LIFO (Last In First Out), la estructura es similar al de una cola:<br>
struct nodo{<br>
int valor;<br>
struct nodo *sig;<br>
struct nodo *tope;<br>
}<br>
Los nodos apuntan al siguiente y al elemento del tope de la pila, es decir, al ultimo elemento, se suele acu�ar las funciones pop y push para agregar y eliminar de la pila respectivamente<br>
Tanto las listas como las pilas, pueden tambi�n definirse como estructuras auto-referenciadas donde, en vez de 2 punteros, s�lo se utiliza uno al siguiente, para este caso se mostrar�n los c�digos de borrado, puesto que el de a�adir es el mismo, para ambos casos<br>
<u>Colas</u><br>
//se debe borrar el nodo que est� en la primera posici�n
/*suponiendo que en el main est� declarado un struct nodo *C; C = null;*/<br>
void borrar_nodo_cola(struct nodo **C){<br>
<br>     
     struct *nodo_aux;<br>
     <br>
     if(*C == NULL)<br>
          printf("No hay datos en la lista\n");<br>
     else<br>
     {     <br>
      nodo_aux = *C;<br>
      *C = (*C)->sig;<br>
      free(nodo_aux);<br>
     }}<br><br>

/*ahor suponiendo que en el main est� declarado un struct nodo *P; P = null;*/
<br>
void borrar_nodo_pila(struct nodo **P){<br>
    <br> 
     struct nodo *nodo_ant, *nodo_rec, *nodo_aux;<br>
     <br>
     if(*P == NULL)<br>
          printf("No hay datos en la pila\n");<br>
     else<br>
     {<br>
      <br>
     nodo_rec = *P;<br>
     <br>
     while (nodo_rec->sig != NULL){<br>
              nodo_ant = nodo_rec;<br>
              nodo_rec = nodo_rec->sig;<br>
              }<br>
          if(nodo_rec != NULL){<br>
          nodo_aux = nodo_rec;<br>
          nodo_ant->sig = nodo_rec->sig;<br>
          free(nodo_aux);       <br>
          }<br>
     }}<br>
</h3>
<h3><a href="#">�rboles</a></h3>
<h3 align="justify">Los �rboles son estructuras con mayor amplitud de usos desde diccionarios de datos hasta toma de decisiones en sistemas avanzados<br>
Los �rboles b�sicamente se componen de nodos (como cualquier TAD), un nodo principal har� de ra�z del arbol, del cual saldr�n los nodos hijos, cada uno de ellos ser� otro nodo.-<br>
Los nodos pueden tener de 0 a n hijos donde n es el n�mero de nodos a�adidos:<br>
struct nodo_arbol{<br>
struct nodo_arbol *hijo1;<br>
struct nodo_arbol *hijo2;<br>
struct nodo_arbol *hijo3;<br>
...<br>
struct nodo_arbol *hijo_n;<br>
};<br>
<br>
<i>Caracter�sticas</i><br>
Tanto la forma de a�adir o eliminar nodos es de manera similar<br> 
Un �rbol tiene definiciones espec�ficas:<br>
Altura: La altura est� determinada por la cantidad de nodos m&aacute;ximos 
verticales que contiene un �rbol<br>
Balance: Un �rbol est� balanceado cuando la diferencia de la suma de los nodos (no su valor) de la derecha con su izquierda tiene como resultado menor a 2<br>
Autobalanceable: Es un tipo de arbol que al agregar o eliminar nodos junto con el algoritmo realiza un conjunto de instrucciones de tal manera que el arbol quede balanceado, dichas instrucciones se llaman rotaciones
<br>
<br>
<u>Formas de recorrer un arbol</u><br>
Al igual que cualquier TAD un �rbol se puede recorrer, pero el orden por el cual es recorrido se definen de distinta manera:<br>
<ul>
<li>Preorden: Ra�z, Sub �rbol izquierdo, Sub �rbol derecho</li>
<li>Inorden: Sub �rbol izquierdo, Ra�z, Sub �rbol derecho</li>
<li>Postorden: Sub �rbol izquierdo, Sub �rbol derecho, Ra�z</li>
</ul>
 
<u>Tipos de �rbol</u><br>
Algunos tipos de �rbol son:
<ul>
<li>Binarios: Son aquellos que en cada nodo tienen como m�ximo 2 nodos hijos</li>
<li>AVL: Son un &aacute;rbol binario autobalanceable</li>
<li>Arbol 2-n: Son �rboles que tienen multiplicidad de ramas, es decir, tienen de 0 a x nodos</li>
<li>B: Son &aacute;rboles 2-n auto balanceables</li>
</ul>
Ahora mostraremos un c�digo de �rbol binario com�n:<br>

struct arbol{<br>
       int valor;<br>
       struct arbol *der;<br>
       struct arbol *izq;<br>
       };<br>

void insertar(struct arbol **A, int dato);<br>
void preorden(struct arbol *A);<br>
void inorden(struct arbol *A);<br>
void postorden(struct arbol *A);<br>
void borrar(struct arbol **A, int val);<br>
void rotacion(struct arbol **A, struct arbol **B);<br>
int encontrar(struct arbol *A, int val);<br>
<br>
void main (){<br>
    <br>
int salida = 0, salida2 = 0, opc, listar_opc;<br>
int ndato, val;<br>
<br>
    struct arbol *A;<br>
    A = NULL;]<br>
<br>
do{<br>
    <br>
printf("1) Insertar\n2) Borrar\n3) Listar\n4) Salir\n=============================\n");<br>
    printf("Seleccione una opcion:\t");<br>
    scanf("%d",&opc);<br>
    switch(opc){<br>
                case 1:<br>                  
                     printf("Ingrese un nuevo n�mero:\t");<br>
                     scanf("%d",&ndato);<br>
                     insertar(&A, ndato);<br>
                     break;<br>
                case 2:<br>
                          do{<br>
                          printf("Seleccione el valor del nodo a borrar:\t");<br>
                          scanf("%d", &val);<br>
                          }while((encontrar(A, val) )  != 0);<br>
                     borrar(&A, val);<br>
                     break;<br>
                case 3:<br>
                     do{<br>
                     printf("1) Preorden\n2) Inorden\n3) Postorden\n4) Salir\n===============\n");<br>
                     printf("Seleccione una opcion:\t");<br>
				scanf("%d",&listar_opc);<br>                     
                     switch(listar_opc){<br>
                     case 1:<br>
                     preorden(A);<br>
                     printf("\n")<br>
                     break;<br>
                     case 2:<br>
                     inorden(A);<br>
                     printf("\n");<br>
                     break;<br>
                     case 3:<br>
                     postorden(A);<br>
                     printf("\n");<br>
                     break;<br>
                     case 4:<br>
                     salida2 = 1;<br>
                     break;<br>
                     default:<br>
                     salida2 = 0;<br>
                     break;<br>
                     }<br>
                     }while(salida2 == 0);<br>
                     break;<br>
                case 4:<br>
                     salida = 1;<br>
                     break;<br>
                default:<br>
                     salida = 0;<br>
                        break;<br>
                }<br>
    }while(salida == 0);<br>
<br>
getchar();<br>
    }<br>
<br>
void insertar(struct arbol **A, int ndato){<br>
     if(*A == NULL){<br>
      *A = (struct arbol *) malloc(sizeof(struct arbol));<br>
           (*A)->valor = ndato;<br>
           (*A)->izq = NULL;<br>
           (*A)->der = NULL;<br>
     }<br>
     else if(ndato < (*A)->valor){<br>
                  insertar(&((*A)->izq), ndato);<br>
                  }<br>
     else if(ndato > (*A)->valor){<br>
                  insertar(&((*A)->der), ndato);<br>
                  }<br>
     else<br>
              printf("Error, no puede haber un valor igual\n");<br>
}<br>

void preorden(struct arbol *A){<br>
     if(A != NULL){<br>
          printf("%d ", A->valor);<br>
          preorden(A->izq);<br>
          preorden(A->der);<br>
          }<br>
     }<br>
<br>
void inorden(struct arbol *A){<br>
          if(A != NULL){<br>
          inorden(A->izq);<br>
          printf("%d ", A->valor);<br>
          inorden(A->der);<br>
          }<br>
     }<br>
     <br>
void postorden(struct arbol *A){<br>
          if(A != NULL){<br>
          postorden(A->izq);<br>
          postorden(A->der);<br>
          printf("%d ", A->valor);<br>
          }<br>
     }<br>
<br>
int encontrar(struct arbol *A, int val){<br>
     if(A == NULL){<br>
          printf("Error, no hay nodos en el arbol\n");<br>
          return 2;<br>
          }<br>
     <br>
     if(A != NULL){<br>
          if(A->valor != val){<br>
          preorden(A->izq);<br>
          preorden(A->der);<br>
          }<br>
          else<br>
          return 1;<br>
          }<br>
     return 0;<br>
     }<br>
<br>
void borrar(struct arbol **A, int val){<br>
     struct arbol *arbol_aux;<br>
     <br>
     if((*A) == NULL){<br>
          printf("Error, no hay nodos en el arbol");<br>
          return;<br>
          }<br>
          <br>
          if((*A)->valor < val)<br>
          borrar(&(*A)->der, val);<br>
          else if((*A)->valor > val)<br>
          borrar(&(*A)->izq, val);<br>
          else if((*A)->valor == val){<br>
                      arbol_aux = *A;<br>         
                      if((*A)->izq == NULL)<br>
                                    *A = (*A)->der;<br>
                      else if((*A)->der == NULL)<br>
                                    *A = (*A)->izq;<br>
                      else<br>
                                    rotacion(&(*A)->izq, &arbol_aux);<br>                    
                      free(arbol_aux);<br>
                      }<br>
     }<br>
     <br>
void rotacion(struct arbol **A, struct arbol **B)<br>
{<br>
  if ((*A)->der != NULL) rotacion(&(*A)->der, B);<br>
  else {<br>
    (*B)->valor = (*A)->valor;<br>
    *B = *A;<br>
    *A = (*A)->izq;<br>
  }<br>
}<br>
Nos detendremos en la funci�n rotaci�n, es una funci�n que realiza el cambio de un nodo por otro, especialmente se utiliza al programar �rboles autobalanceados, la rotaci�n puede ocurrir hacia la izquierda (como este caso), como a la derecha
</h3>
 <h3><a href="#">Librer�as adicionales (creaci�n)</a></h3>
<h3 align="justify">Las librer�as son los archivos de cabecera que en escencia son un conjunto de funciones, procedimientos y constantes predeterminadas (no ahondaremos en cada una de ellas), por lo general tienen una extensi�n .h, un ejemplo de ellas es 'stdio.h' que tiene diversas funciones como printf, scanf, fscanf, puts, etc. Por esa raz�n antes de empezar el programa se escribe #include &lt;stdio.h>, es decir, incluimos las funciones b�sicas de entrada/salida est�ndar<br>
<b>C�mo crear librer�as propias y anexarlas</b><br>
Como ya se tienen el entedimiento de c�mo se realizan las funciones, crearemos 3 funciones;<br>
1) Una que distinga que caracter es el valor mayor y que devuelva char<br>
2) Una que convierta de hexadecimal a decimal<br>
3) Una que convierta de binario a decimal<br>
char mayor(char, char);<br>
void hexdec(char[]);<br>
void bindec(char[]);<br>
Los m�todos se guardar�n en un archivo llamado 'lib.h'. Ahora para a�adirla a un archivo .c, se debe incluir como toda librer�a, con la salvedad de que en algunos entornos de desarrollo es ingresada entre comillas (#include "lib.h") y se ejecutan junto con las otras librer�as; Ejemplo:<br>
#include &lt;stdio.h&gt;<br>
#include &lt;math.h&gt;<br>
#include 'lib.h'<br>
void main(){<br>
...<br>
}<br>
<b><u>NOTA:</u></b> No se recomienda nombrar los archivos de librer�a con el mismo nombre de una librer�a est�ndar (ej: stdlib.h), o incluir dentro de dicho archivo funciones y/o procedimientos involucrados dentro de librer�as est�ndar (como pow, printf, strcmp)<br>
</h3>
 <h3><a href="#">Librer�as no est�ndar (compilaci�n)</a></h3>
<h3 align="justify">Se refieren a librer�as no comunes que determinan funciones que realmente son �tiles que no se encuentren dentro de las librer�as est�ndar, como por ejemplo, el uso de interfaz gr�fica dentro de C, un ejemplo muy com�n es la librer�a conio.h, dos.h para windows o ncurses.h para GNU/Linux, que realizan funciones gr�ficas dentro de cada sistema operativo exclusivamente<br>
Para incluir librer�as que no se ncuentran dentro de las est�ndar habr� que agregar a la l�nea de comandos (cmd o terminal), la siguiente instrucci�n (cabe aclarar de antemano que es una instrucci�n gen�rica):<br><br>
Sabemos que se compila b�sicamente:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
gcc -o (nombre) (codigo.c)<br>
Ahora agregamos:<br>
gcc -o (nombre) (codigo.c) -llibreria1.h -llibreria2 ...<br>
En los entornos de desarrollo suelen permiitir en las opciones del compilador, agregar instrucciones secundarias (como en este caso agregar librerias adicionales)</h3>
 <h3><a href="#">Entorno gr�fico</a></h3>
<h3 align="justify">El arte ascii es �til hasta cierto punto, el problema es cuando queremos representar el entorno gr�fico o programar juegos en C, hay una variada distribucion de librer�as gr�ficas, desde s�lo crear gr�ficos, hasta controlar los botones de un joystick, por esa raz�n no hay en esta secci�n, cada librer�a consta de ciertos m�todos que casi nunca son compartidos, por lo que inclinarse s�lo por una librer�a ser�a demasiado injusto, como cualquier librer�a no est�ndar puede enlazarse de las 2 maneras</h3>
 <h3><a href="#">Enlazar MySQL</a></h3>
<h3 align="justify">(Pr�ximamente)<!--Toda empresa guarda sus registros con una base de datos, y donde haya una base de datos, habr� un programa que las administre, como recomendaci�n antes de ver esta secci�n saber operaciones b�sicas de un DBMS, usaremos MySQL<br>
Primero suponemos que tenemos el compilador y mysql instalado, ahora instalamos el conector de C de MySQL--></h3>
</div>
</div>
<div id='tabs-3'>
		<p>Curso de CGI en C</p>
  <div id="secc">
  <h3> <a href="#">Primera aproximaci�n</a></h3>
  <h3 align="justify">Se define como CGI a una sigla Comon Gateway Interface, por lo tanto, no es espec�fico de un lenguaje de programaci�n.<br>
El CGI, se ejcutar� dentro de un servidor para dar una respuesta espec�fica<br>
Para poder ejecutar un c�digo CGI, debemos tener nuestro servidor instalado y configurado para poder ejecutar c�digo CGI.<br>
Como primer ejemplo ser� el hola mundo:<br>
#include &lt;stdio.h><br>
void main(){<br>
     printf("Content-Type: text/html\n\n");<br>
     printf("Hola mundo\n");<br>
    }<br>
Para poder ejecutar u n c�digo CGI de este tipo debe ser primero compilado, para poder ser interpretado por el servidor, se compila como cualquier archivo .c, con la salvedad:<br>
gcc (nombre_archivo.c) -o (nombre_salida.cgi)<br>
Luego es ubicado dentro del servidor local para ser visto dentro de un navegador
</h3>
<h3> <a href="#">Realizar respuestas</a></h3>
  <h3 align="justify">Tendremos que hacer uso de un formulario con un m�todo que ejecutar� al ingresar los datos<br>
</h3>
  	</div>
	</div>
	
</div>

</div>
 </div>

</center>


</body>
</html>