<?xml version="1.0"?> 
     
<xsl:stylesheet version="1.0" xmlns:xsl= "http://www.w3.org/TR/WD-xsl"> 
 
<xsl:template match="/">   
<html> 
  <head> 
  <title>Stocks</title> 
  </head> 
  <body bgcolor="#ffffcc"> 
    <xsl:apply-templates/> 
  </body> 
  </html> 
</xsl:template> 
 
<xsl:template match="inventario"> 
 
   <table border="2" width="75%"> 
     <tr> 
      <th><U>Nombre</U></th> 
      <th><U>Cantidad</U></th> 
      <th><U>Precio</U></th> 
      <th><U>Stock</U></th> 
	  <th><U>Imagen</U></th> 
     </tr> 
     <xsl:for-each select="producto"> 
       <tr> 
         <td> 
           <i><xsl:value-of select="nombre"/></i> 
         </td> 
       <td> 
          <xsl:value-of select="cantidad"/> 
        </td> 
       <td> 
          <xsl:value-of select="precio"/> 
        </td> 
       <td> 
          <xsl:value-of select="stock"/> 
        </td> 
       <td> 
          <xsl:value-of select="imagen"/> 
        </td>
        </tr> 
      </xsl:for-each> 
    </table> 
  </xsl:template> 
 
</xsl:stylesheet>