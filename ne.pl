#!/usr/bin/perl

use Mysql;
print "Content-type: text/html \n\n";

$host = "localhost";
$database = "post";
$tablename = "postit";
$user = "root";
$pw = "toor";

# PERL MYSQL CONNECT()
$connect = Mysql->connect($host, $database, $user, $pw);

# SELECT DB
$connect->selectdb($database);

# DEFINE A MySQL QUERY
$myquery = "SELECT * FROM $tablename";

# EXECUTE THE QUERY FUNCTION
$execute = $connect->query($myquery);

# HTML TABLE
print "<table border='1'><tr>
<th>id</th>
<th>product</th>
<th>quantity</th></tr>";

# FETCHROW ARRAY

while (@results = $execute->fetchrow()) {
	print "<tr><td>"
	.$results[0]."</td><td>"
	.$results[1]."</td><td>"
	.$results[2]."</td></tr>";
}

print "</table>";